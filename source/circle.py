from math import pi


def circle_area(r):

    if type(r) not in [int, float]:
        raise  TypeError("Radius must be non negative real number")

    if r < 0:
        raise ValueError("Radius must not be negative")

    return pi*(r**2)
